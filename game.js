
var level;
var level1;
var level2;
var level3;

var menuState = {
    preload: function() {
        game.load.image('bg', 'Assets/menu_bg.jpg');
    },
    create: function() {
        game.add.image(0, 0, 'bg');

        /// text
        var nameLabel = game.add.text(game.world.centerX, game.world.centerY - 50, 'Raiden', { font: '84px Arial', fill: '#ffffff' }); 
        nameLabel.anchor.setTo(0.5, 0.5);
        var levelLabel = game.add.text(game.world.centerX, game.world.centerY + 100, 'Press 123 to Start', { font: '50px Arial', fill: '#ffffff' }); 
        levelLabel.anchor.setTo(0.5, 0.5);

        /// level
        level1 = game.input.keyboard.addKey(Phaser.KeyCode.ONE);
        level2 = game.input.keyboard.addKey(Phaser.KeyCode.TWO);
        level3 = game.input.keyboard.addKey(Phaser.KeyCode.THREE);
    },

    update: function() {
        if (level1.isDown){
            level = 1;
            this.start();
        }
        else if (level2.isDown){
            level = 2;
            this.start();
        }
        else if (level3.isDown){
            level = 3;
            this.start();
        }
    },

    start: function() {
        game.state.start('play');
    },
};

var bg;
var player;
var enemies;
var boss;
var HitBossNumber = 0;
var bullets;
var bulletTime = 0;
var enemyBullets;
var firingTimer = 0;
var weapon2;
var weapon2_time = 0;
var bomb;
var bomb_number = 0;

var helper;
var helper_time = 0;
var helper_bulletTime = 0;

var explosions;
var emitter;

var cursors;
var fireButton;
var score = 0;
var scoreString = '';
var scoreText;
var lives;
var stateText;
var livingEnemies = [];

var BGM;
var boom;
var shoot1;

var restartButton;
var pauseLabel;
var volumeUp;
var volumeDown;
var GameOver = 0;

var playState = {
    preload: function() {
        game.load.image('bullet', 'Assets/bullet.png');
        game.load.image('enemyBullet', 'Assets/enemy-bullet.png');
        game.load.spritesheet('invader', 'Assets/invader1.png');
        game.load.spritesheet('boss', 'Assets/Boss.png');
        game.load.image('ship', 'Assets/player.png');
        game.load.spritesheet('player', 'Assets/ship.png', 64.5, 54);
        game.load.spritesheet('kaboom', 'Assets/explode.png', 128, 128);
        game.load.image('starfield', 'Assets/starfield.png');
        game.load.image('weapon2', 'Assets/03.png');
        game.load.image('bomb', 'Assets/bomb.png');
        game.load.spritesheet('helper', 'Assets/attackship.png');
        game.load.image('pixel', 'Assets/pixel.png');
        
        game.load.audio('bgm', 'Assets/bgm.mp3');
        game.load.audio('boom', 'Assets/boom.mp3');
        game.load.audio('shoot1', 'Assets/shoot1.mp3');
    },
    create: function() {
        score = 0;
        GameOver = 0;
        HitBossNumber = 0;
        bomb_number = 0;
        helper_time = 0;

        game.physics.startSystem(Phaser.Physics.ARCADE);

        // background
        bg = game.add.tileSprite(0, 0, 800, 600, 'starfield');
        bg.autoScroll(0, 200);

        // music
        BGM = game.add.audio('bgm');
        BGM.play();
        boom = game.add.audio('boom');
        shoot1 = game.add.audio('shoot1');
        volumeUp = game.add.text(50, game.height - 50, 'Up', {
            font: '24px Arial', fill: '#fff'
        });
        volumeUp.inputEnabled = true;
        volumeUp.events.onInputUp.add(function(){
            BGM.volume += 0.3;
        });

        volumeDown = game.add.text(100, game.height - 50, 'Down', {
            font: '24px Arial', fill: '#fff'
        });
        volumeDown.inputEnabled = true;
        volumeDown.events.onInputUp.add(function(){
            BGM.volume -= 0.3;
        });

        // player
        player = game.add.sprite(400, 500, 'player');
        player.animations.add('rightfly', [4], 8, true);
        player.animations.add('leftfly', [3], 8, true);
        player.anchor.setTo(0.5, 0.5);
        game.physics.enable(player, Phaser.Physics.ARCADE);

        // player bullets
        bullets = game.add.group();
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        bullets.createMultiple(50, 'bullet');
        bullets.setAll('anchor.x', 0.5);
        bullets.setAll('anchor.y', 1);
        bullets.setAll('outOfBoundsKill', true);
        bullets.setAll('checkWorldBounds', true);

        // enemies
        enemies = game.add.group();
        enemies.enableBody = true;
        enemies.physicsBodyType = Phaser.Physics.ARCADE;
        enemies.createMultiple(50, 'invader');
        game.time.events.loop(500, this.createEnemies, this);

        // enemies bullets
        enemyBullets = game.add.group();
        enemyBullets.enableBody = true;
        enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        enemyBullets.createMultiple(50, 'enemyBullet');
        enemyBullets.setAll('anchor.x', 0.5);
        enemyBullets.setAll('anchor.y', 1);
        enemyBullets.setAll('outOfBoundsKill', true);
        enemyBullets.setAll('checkWorldBounds', true);

        // boss
        boss = game.add.group();
        boss.enableBody = true;
        boss.physicsBodyType = Phaser.Physics.ARCADE;
        boss.createMultiple(1, 'boss');
        game.time.events.loop(500, this.createBoss, this);

        // weapon
        weapon2 = game.add.sprite(60, 510, 'weapon2');
        game.physics.arcade.enable(weapon2);
        weapon2.scale.setTo(0.5, 0.5);
        //weapon2.body.velocity.x = 10 * game.rnd.pick([-1, 1]);
        //weapon2.body.velocity.y = 10 * game.rnd.pick([-1, 1]);
        //weapon2.outOfBoundsKill = true;
        //weapon2.checkWorldBounds = true;

        // bomb
        bomb = game.add.sprite(300, 240, 'bomb');
        game.physics.arcade.enable(bomb);
        bomb.scale.setTo(0.05, 0.05);

        // helper
        helper = game.add.sprite(200, 200, 'helper');
        helper.angle = -90;
        game.physics.arcade.enable(helper);
        helper.scale.setTo(0.15, 0.15);
        helper.body.velocity.x = 10 * game.rnd.pick([-1, 1]);
        helper.body.velocity.y = 10 * game.rnd.pick([-1, 1]);

        // explosion
        explosions = game.add.group();
        explosions.createMultiple(30, 'kaboom');
        explosions.forEach(this.setupInvader, this);

        // emitter
        emitter = game.add.emitter(0, 0, 15);
        emitter.makeParticles('pixel');
        emitter.setYSpeed(-150, 150);
        emitter.setXSpeed(-150, 150);
        emitter.setScale(2, 0, 2, 0, 800);
        emitter.gravity = 0;

        // score
        scoreString = 'Score : ';
        scoreText = game.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });

        // lives
        lives = game.add.group();
        game.add.text(game.world.width - 200, 10, 'Lives : ', { font: '34px Arial', fill: '#fff' });
        for (var i = 0; i < 3; i++){
            var ship = lives.create(game.world.width - 100 + (30 * i), 60, 'ship');
            ship.anchor.setTo(0.5, 0.5);
            ship.angle = 90;
            ship.alpha = 0.4;
        }

        // text
        stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
        stateText.anchor.setTo(0.5, 0.5);
        stateText.visible = false;

        // another
        cursors = game.input.keyboard.createCursorKeys();
        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        boomButton = game.input.keyboard.addKey(Phaser.KeyCode.G);
        restartButton = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        pauseLabel = game.add.text(game.width - 100, game.height - 50, 'Pause', {
            font: '24px Arial', fill: '#fff'
        });
        pauseLabel.inputEnabled = true;
        pauseLabel.events.onInputUp.add(function(){
            game.paused = true;
        });
        game.input.onDown.add(unpause, self);

        function unpause(event) {
            if (game.paused){
                game.paused = false;
            }
        };
    },

    update: function() {
        game.physics.arcade.overlap(bullets, enemies, this.collisionHandler, null, this);
        game.physics.arcade.overlap(bullets, boss, this.bulletHitsBoss, null, this);
        game.physics.arcade.overlap(enemyBullets, player, this.enemyHitsPlayer, null,);
        game.physics.arcade.overlap(player, enemies, this.playerCollision, null,);
        game.physics.arcade.overlap(player, weapon2, this.takeWeapon, null,);
        game.physics.arcade.overlap(player, bomb, this.takeBomb, null,);
        game.physics.arcade.overlap(player, helper, this.takeHelper, null,);
        
        this.controlPlayer();
        if (game.time.now > firingTimer){
            this.enemyFires();
            enemies.forEachAlive(function(enemy){
                if (level == 1){
                    enemy.body.velocity.x = 200 * game.rnd.pick([-1, 1]);
                    enemy.body.velocity.y = 200;
                }  
                else if (level == 2){
                    enemy.body.velocity.x = 300 * game.rnd.pick([-1, 1]);
                    enemy.body.velocity.y = 300;
                }  
                else if (level == 3){
                    enemy.body.velocity.x = 400 * game.rnd.pick([-1, 1]);
                    enemy.body.velocity.y = 400;
                }  
            });

            this.bossFires();
            boss.forEachAlive(function(Boss){
                if (level == 1)
                    Boss.body.velocity.x = 30 * game.rnd.pick([-1, 1]); 
                else if (level == 2)
                    Boss.body.velocity.x = 60 * game.rnd.pick([-1, 1]);
                else if (level == 3)
                    Boss.body.velocity.x = 100 * game.rnd.pick([-1, 1]);
            });
        }

        if (restartButton.isDown && GameOver == 1)
            this.restart();
    },

    createEnemies: function() {
        if (score < 150){
            var enemy = enemies.getFirstDead(); 
            if (!enemy) { return;} 
            enemy.angle = 180;
            enemy.scale.setTo(0.4, 0.4);
            enemy.anchor.setTo(0.5, 1); 
            enemy.reset(game.width/2, 0); 
            enemy.body.velocity.x = 200 * game.rnd.pick([-1, 1]);
            enemy.body.velocity.y = 200 * game.rnd.pick([-1, 1]);  
            enemy.checkWorldBounds = true; 
            enemy.outOfBoundsKill = true; 
        }
    },

    createBoss: function() {
        if (score >= 150 && HitBossNumber < 10){
            var Boss = boss.getFirstDead(); 
            if (!Boss) { return;}
            Boss.angle = 180;
            Boss.scale.setTo(0.3, 0.3);
            Boss.anchor.setTo(0.5, 1); 
            Boss.reset(game.width/2, 50); 
            Boss.checkWorldBounds = true; 
            Boss.outOfBoundsKill = true;
        } 
    },

    setupInvader: function(invader) {
        invader.anchor.x = 0.5;
        invader.anchor.y = 0.5;
        invader.animations.add('kaboom');
    },

    enemyFires: function() {
        //  Grab the first bullet we can from the pool
        enemyBullet = enemyBullets.getFirstExists(false);

        livingEnemies.length=0;

        enemies.forEachAlive(function(enemy){
            // put every living enemy in an array
            livingEnemies.push(enemy);
        });

        if (enemyBullet && livingEnemies.length > 0){    
            var random=game.rnd.integerInRange(0,livingEnemies.length-1);

            // randomly select one of them
            var shooter=livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);

            //game.physics.arcade.moveToObject(enemyBullet,player,500);
            if (level == 1){
                game.physics.arcade.moveToObject(enemyBullet,player,300);
                firingTimer = game.time.now + 1500;
            }
            else if (level == 2){
                game.physics.arcade.moveToObject(enemyBullet,player,600);
                firingTimer = game.time.now + 900;
            }
            else if (level == 3){
                game.physics.arcade.moveToObject(enemyBullet,player,1000);
                firingTimer = game.time.now + 500;
            }
        }
    },

    bossFires: function() {
        //shoot1.play();
        if (score >= 50){
            boss.forEachAlive(function(Boss){
                for (var i = 1; i <= 5; i++){
                    enemyBullet = enemyBullets.getFirstExists(false);
                    //enemyBullet.reset(Boss.body.x, Boss.body.y);
                    if (enemyBullet){
                        if (i == 1){
                            enemyBullet.reset(Boss.x, Boss.y + 30);
                            enemyBullet.body.velocity.y = 400;
                        }
                        else if (i == 2){
                            enemyBullet.reset(Boss.x, Boss.y + 30);
                            enemyBullet.body.velocity.x = -400;
                            enemyBullet.body.velocity.y = 400;
                        }
                        else if (i == 3){
                            enemyBullet.reset(Boss.x, Boss.y + 30);
                            enemyBullet.body.velocity.x = 400;
                            enemyBullet.body.velocity.y = 400;
                        }
                        else if (i == 4){
                            enemyBullet.reset(Boss.x, Boss.y + 30);
                            enemyBullet.body.velocity.x = 400;
                            //enemyBullet.body.velocity.y = 400;
                        }
                        else if (i == 5){
                            enemyBullet.reset(Boss.x, Boss.y + 30);
                            enemyBullet.body.velocity.x = -400;
                           // enemyBullet.body.velocity.y = 400;
                        }
                    }
                }
            });
            if (level == 1)   
                firingTimer = game.time.now + 800;
            else if (level == 2)
                firingTimer = game.time.now + 500;
            else if (level == 3)
                firingTimer = game.time.now + 300;

        }
    },

    takeWeapon: function() {
        weapon2_time = game.time.now + 5000;
        var weapon2Position = [{x: 140, y: 60}, {x: 360, y: 60}, {x: 190, y: 300}, {x: 440, y: 400}, {x: 520, y: 300}, {x: 620, y: 550}]; 
        for (var i = 0; i < weapon2Position.length; i++){
            if (weapon2Position[i].x == weapon2.x)
                weapon2Position.splice(i, 1);
        }
        var newPosition = game.rnd.pick(weapon2Position);
        weapon2.reset(newPosition.x, newPosition.y);
        //weapon2.body.velocity.x = 10 * game.rnd.pick([-1, 1]);
        //weapon2.body.velocity.y = 10 * game.rnd.pick([-1, 1]);
    },

    takeBomb: function() {
        bomb_number++;
        var bombPosition = [{x: 200, y: 60}, {x: 160, y: 400}, {x: 510, y: 110}, {x: 540, y: 561}, {x: 40, y: 350}, {x: 720, y: 410}]; 
        for (var i = 0; i < bombPosition.length; i++){
            if (bombPosition[i].x == bomb.x)
                bombPosition.splice(i, 1);
        }
        var newPosition = game.rnd.pick(bombPosition);
        bomb.reset(newPosition.x, newPosition.y);
    },

    takeHelper: function() {
        helper_time = game.time.now + 1000;
    },

    collisionHandler: function(bullet, enemy) {
        //  When a bullet hits an alien we kill them both
        boom.play();
        bullet.kill();
        enemy.kill();
        //  Increase the score
        score += 20;
        scoreText.text = scoreString + score;

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(enemy.body.x, enemy.body.y);
        explosion.play('kaboom', 30, false, true);
    },

    bulletHitsBoss: function(bullet, Boss) {
        HitBossNumber++;
        if (HitBossNumber >= 10){
            var explosion = explosions.getFirstExists(false);
            explosion.reset(Boss.body.x, Boss.body.y);
            explosion.play('kaboom', 30, false, true);

            Boss.kill();
            enemyBullets.callAll('kill');

            stateText.text = "           You Win \n Press Enter to restart";
            stateText.visible = true;

            GameOver = 1;
        }

        bullet.kill();
        emitter.x = Boss.x;
        emitter.y = Boss.y;
        emitter.start(true, 800, null, 15);
    },

    enemyHitsPlayer: function(player, bullet) {
        boom.play();

        bullet.kill();

        live = lives.getFirstAlive();

        if (live){
            live.kill();
        }

        emitter.x = player.x;
        emitter.y = player.y;
        emitter.start(true, 800, null, 15);

        if (lives.countLiving() < 1){
            var explosion = explosions.getFirstExists(false);
            explosion.reset(player.body.x, player.body.y);
            explosion.play('kaboom', 30, false, true);

            player.kill();
            enemyBullets.callAll('kill');

            stateText.text = "           You Lose \n Press Enter to restart";
            stateText.visible = true;

            GameOver = 1;
        }
    },

    playerCollision: function(player, enemy) {
        boom.play();

        enemy.kill();

        live = lives.getFirstAlive();

        if (live){
            live.kill();
        }

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(player.body.x, player.body.y);
        explosion.play('kaboom', 30, false, true);

        if (lives.countLiving() < 1){
            var explosion = explosions.getFirstExists(false);
            explosion.reset(player.body.x, player.body.y);
            explosion.play('kaboom', 30, false, true);

            player.kill();
            enemyBullets.callAll('kill');

            stateText.text = "           You Lose \n Press Enter to restart";
            stateText.visible = true;

            GameOver = 1;
        }
    },

    playerDie: function() { game.state.start('play');},

    controlPlayer: function() {
        player.body.velocity.x = 0;
        if (cursors.left.isDown && player.x > player.width/2){ 
            player.body.velocity.x = -200;
            player.animations.play('leftfly');
        }
        else if (cursors.right.isDown && player.x < game.width - player.width/2){ 
            player.body.velocity.x = 200;
            player.animations.play('rightfly');
        }
        else if (cursors.up.isDown && player.y > player.height/2) 
            player.body.velocity.y = -200;
        else if (cursors.down.isDown && player.y < game.height - player.height/2) 
            player.body.velocity.y = 200;
        else{
            player.body.velocity.x = 0;
            player.body.velocity.y = 0;
            player.animations.stop();
            player.frame = 0;
        }

        if (fireButton.isDown){
            if (game.time.now  < weapon2_time)
                this.fireBullet_2();
            else 
                this.fireBullet();
        }
        if (game.time.now < helper_time){
            helper.x = player.x - 50;
            helper.y = player.y -50;
            this.helper_fireBullet();
        }

        if (boomButton.isDown){this.bigBang();}
    },

    helper_fireBullet: function() {
        if (game.time.now > helper_bulletTime){
            shoot1.play();

            bullet = bullets.getFirstExists(false);

            if (bullet){
                bullet.reset(helper.x, helper.y + 8);
                bullet.body.velocity.y = -400;
                helper_bulletTime = game.time.now + 200;
                bullet.angle = 0;
            }
        }
    },

    fireBullet: function() {
        if (game.time.now > bulletTime){
            shoot1.play();

            bullet = bullets.getFirstExists(false);

            if (bullet){
                bullet.reset(player.x, player.y + 8);
                bullet.body.velocity.y = -400;
                bulletTime = game.time.now + 200;
                bullet.angle = 0;
            }
        }
    },

    fireBullet_2: function() {
        if (game.time.now > bulletTime){
            shoot1.play();
            for (var i = 1; i <= 3; i++){
                bullet = bullets.getFirstExists(false);

                if (bullet){
                    if (i == 1){
                        bullet.reset(player.x, player.y + 8);
                        bullet.body.velocity.y = -400;
                        bullet.angle = 0;
                    }
                    else if (i == 2){
                        bullet.reset(player.x, player.y + 8);
                        bullet.body.velocity.x = -400;
                        bullet.body.velocity.y = -400;
                        bullet.angle = -45;
                    }
                    else if (i == 3){
                        bullet.reset(player.x, player.y + 8);
                        bullet.body.velocity.x = 400;
                        bullet.body.velocity.y = -400;
                        bullet.angle = 45;
                    }
                }
            }
            bulletTime = game.time.now + 200;
        }
    },

    bigBang: function() {
        if (bomb_number > 0){
            enemies.forEachAlive(function(enemy){
                boom.play();
                enemy.kill();
                //  Increase the score
                score += 20;
                scoreText.text = scoreString + score;

                //  And create an explosion :)
                var explosion = explosions.getFirstExists(false);
                explosion.reset(enemy.body.x, enemy.body.y);
                explosion.play('kaboom', 30, false, true);
            });
            bomb_number--;
        }
    },

    restart: function() {
        game.state.start('menu');
    },
};

var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');

game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.start('menu');



