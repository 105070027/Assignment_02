# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : [一開始在menu時，可以按1或2或3進入難度不同的遊戲，難度越高敵人移動速度、射擊速度也越快;在遊戲中若吃到炸彈，按下G鍵可以消滅所有敵人(除了BOSS)]
2. Animations : [玩家左右移動時有不同的frame]
3. Particle Systems : [敵人子彈打到玩家和玩家子彈打到BOSS會有emitter的效果]
4. Sound effects : [有BGM、射擊音效及爆炸音效，按遊戲中左下角的Up和Down可以調整BGM音量大小]
5. Leaderboard : [沒做]

# Bonus Functions Description : 
1. [Unique bullet] : [遊戲中若吃到鎧甲可以在一定時間內一次發射三發]
2. [Little helper] : [遊戲中若吃到小飛機，它會持續跟在玩家身邊並攻擊敵人]
3. [BOSS] : [當分數到達150後，會出現BOSS，BOSS會左右移動並扇形狀發射子彈，打到它10下後即可獲得勝利]
